package Data::Vault;
use 5.008001;
use strict;
use warnings;

our $VERSION = "0.01";



1;
__END__

=encoding utf-8

=head1 NAME

Data::Vault - It's new $module

=head1 SYNOPSIS

    use Data::Vault;

=head1 DESCRIPTION

Data::Vault is ...

=head1 LICENSE

Copyright (C) Bernhard Graf.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 AUTHOR

Bernhard Graf E<lt>augensalat@gmail.comE<gt>

=cut

