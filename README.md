[![Gitlab pipeline](https://gitlab.com/augensalat/perl-data-vault/badges/develop/pipeline.svg)](https://gitlab.com/augensalat/perl-data-vault/-/commits/develop) [![CryptCPAN Release](https://badge.fury.io/pl/Data-Vault.svg)](https://metacpan.org/release/Data-Vault)
# NAME

Data::Vault - It's new $module

# SYNOPSIS

    use Data::Vault;

# DESCRIPTION

Data::Vault is ...

# LICENSE

Copyright (C) Bernhard Graf.

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# AUTHOR

Bernhard Graf <augensalat@gmail.com>
